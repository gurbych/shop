#include "Item.h"

std::set<Item*> Item::itemList {};
int Item::lastItemId = 0;

Item::Item(Category* cat, const std::string& itemName) {
    this->itemId = lastItemId;
    lastItemId += 1;
    itemList.insert(this);
    this->cat = cat;
    this->itemName = itemName;

    cat->addItem(this);
}

Item::~Item() {
    cat = NULL;
}

Category* Item::getCat() const {
    return cat;
}

const std::string& Item::getName() const {
    return itemName;
}

const std::set<Item*>& Item::getList() {
    return itemList;
}

const int Item::getId() const {
    return itemId;
}

void Item::changeCategory(Category* newCategory) {
    if ( newCategory == cat ) {
        return;
    }

    cat->removeItem(this);
    cat->addItem(this);

    cat = newCategory;
}

std::ostream& operator<<(std::ostream& out, const std::set<Item*> item) {
    std::set<Item*>::iterator it = item.begin();

    out << "List of items:" << '\n';
    for ( ; it != item.end(); it++ ) {
        out << (*it)->getName() << " (ID: " << (*it)->getId() << ", Cat: " << (*it)->getCat()->getName() << ")" << std::endl;
    }

    return out;
}