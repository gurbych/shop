#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <iostream>
#include <set>
#include "Order.h"

class Order;

class Customer {
    private:
        static int lastCustomerId;
        int customerId;
        static std::set<Customer*> customerList;
        std::set<Order*> orders;
        std::string name;

    public:
        Customer(const std::string& name);
        ~Customer();

        const std::string& getName() const;
        const std::set<Order*>& getOrders() const;
        static const std::set<Customer*>& getList();
        const int getId() const;

        void addOrder(Order* newOrder);
        void removeOrder(Order* oldOrder);
};

std::ostream& operator<<(std::ostream& out, const Customer& customer);

std::ostream& operator<<(std::ostream& out, const std::set<Customer*> customer);

#endif //Customer_H