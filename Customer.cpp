#include "Customer.h"

std::set<Customer*> Customer::customerList {};
int Customer::lastCustomerId = 0;

Customer::Customer(const std::string& name) : name(name) {
    this->customerId = lastCustomerId;
    lastCustomerId += 1;
    customerList.insert(this);
}

Customer::~Customer() {
    std::set<Order*>::iterator it = orders.begin();

    for ( ; it != orders.end(); ++it ) {
        delete (*it);
    }
    orders.clear();
}

const std::string& Customer::getName() const {
    return name;
}

const std::set<Order*>& Customer::getOrders() const {
    return orders;
}

const std::set<Customer*>& Customer::getList() {
    return customerList;
}

const int Customer::getId() const {
    return customerId;
}

void Customer::addOrder(Order* newOrder) {
    if ( newOrder->getCustomer() != this ) {
        return;
    }
    orders.insert(newOrder);
}

void Customer::removeOrder(Order* oldOrder) {
    orders.erase(oldOrder);
}

std::ostream& operator<<(std::ostream& out, const Customer& customer) {
    out << customer.getName() << ":" << std::endl;

    std::set<Order*>::iterator it = customer.getOrders().begin();
    for ( ; it != customer.getOrders().end(); it++ ) {
        out << "--" << (*it)->getId() << std::endl;
    }
    return out;
}

std::ostream& operator<<(std::ostream& out, const std::set<Customer*> customer) {
    std::set<Customer*>::iterator it = customer.begin();

    out << "List of customers:" << '\n';
    for ( ; it != customer.end(); it++ ) {
        out << (*it)->getName() << " (ID: " << (*it)->getId() << ")" << std::endl;
    }

    return out;
}
