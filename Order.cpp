#include "Order.h"

std::set<Order*> Order::orderList {};
int Order::lastOrderId = 100500;

Order::Order(Customer* customer, Item* item) {
    this->orderId = lastOrderId;
    lastOrderId += 1;
    orderList.insert(this);
    this->customer = customer;
    items.insert(item);

    customer->addOrder(this);
}

Order::~Order() {
    customer = NULL;
}

Customer* Order::getCustomer() const {
    return customer;
}

const std::set<Order*>& Order::getList() {
    return orderList;
}

const int Order::getId() const {
    return orderId;
}

const std::set<Item*>& Order::getItems() const {
    return items;
}

void Order::addItem(Item* newItem) {
    items.insert(newItem);
    Item::itemList.insert(newItem);
}

void Order::removeItem(Item* oldItem) {
    items.erase(oldItem);
    Item::itemList.erase(oldItem);
}

std::ostream& operator<<(std::ostream& out, const Order& order) {
    out << order.getCustomer()->getName() << "'s order:" << '\n';

    std::set<Item*>::iterator it = order.getItems().begin();
    for ( ; it != order.getItems().end(); it++ ) {
        out << "--" << (*it)->getName() << std::endl;
    }
    return out;
}

std::ostream& operator<<(std::ostream& out, const std::set<Order*> order) {
    std::set<Order*>::iterator it = order.begin();

    out << "List of orders:" << '\n';
    for ( ; it != order.end(); it++ ) {
        out << "Order ID: " << (*it)->getId() << ", Customer: " << (*it)->getCustomer()->getName() << std::endl;
    }

    return out;
}