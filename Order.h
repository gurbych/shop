#ifndef ORDER_H
#define ORDER_H

#include <iostream>
#include "Customer.h"
#include "Item.h"

class Customer;

class Order {
    private:
    	static int lastOrderId;
    	int orderId;
    	static std::set<Order*> orderList;
        Customer* customer;
        std::set<Item*> items;

    public:
        Order(Customer* customer, Item* item);
        ~Order();

        Customer* getCustomer() const;
        const std::set<Item*>& getItems() const;
        static const std::set<Order*>& getList();
        const int getId() const;

        void addItem(Item* newItem);
        void removeItem(Item* oldItem);
};

std::ostream& operator<<(std::ostream& out, const Order& order);

std::ostream& operator<<(std::ostream& out, const std::set<Order*> order);

#endif //Order_H