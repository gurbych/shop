#include <iostream>
#include "Item.h"
#include "Category.h"
#include "Customer.h"
#include "Order.h"

int main() {
    Category* notebook = new Category("Notebooks");

    Item* iPadPro = new Item(notebook, "iPad Pro");
    Item* iPadAir = new Item(notebook, "iPad Air");
    Item* lenovo = new Item(notebook, "Lenovo");
    Item* hp = new Item(notebook, "HP ProBook");

    Category* mobilePhone = new Category("Mobile phones");

    Item* iPhone5 = new Item(mobilePhone, "iPhone 5");
    Item* iPhone5S = new Item(mobilePhone, "iPhone 5S");
    Item* gSmart = new Item(mobilePhone, "G Smart Guru 2");
    Item* samsung = new Item(mobilePhone, "Galaxy 3");

    Customer* virginia = new Customer("Virginia");

    Order* order1 = new Order(virginia, iPadPro);

    order1->addItem(iPadAir);

    std::cout << *order1 << std::endl;

    std::cout << Category::getList() << std::endl;
    std::cout << Item::getList() << std::endl;
    std::cout << Customer::getList() << std::endl;
    std::cout << Order::getList() << std::endl;

	delete notebook;
	delete mobilePhone;

    return 0;
}