#ifndef ITEM_H
#define ITEM_H

#include <iostream>
#include "Category.h"

class Category;

class Item {
    private:
    	static int lastItemId;
    	int itemId;
        Category* cat;
        std::string itemName;

    public:
        static std::set<Item*> itemList;
        
        Item(Category* cat, const std::string& itemName);
        ~Item();

        Category* getCat() const;
        const std::string& getName() const;
        static const std::set<Item*>& getList();
        const int getId() const;

        void changeCategory(Category* newCat);
};

std::ostream& operator<<(std::ostream& out, const std::set<Item*> item);

#endif //Item_H