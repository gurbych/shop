#ifndef CATEGORY_H
#define CATEGORY_H

#include <iostream>
#include <set>
#include "Item.h"

class Item;

class Category {
    private:
        static int lastCategoryId;
        int catId;
        static std::set<Category*> categoryList;
        std::set<Item*> items;
        std::string name;

    public:
        Category(const std::string& name);
        ~Category();

        const std::string& getName() const;
        const std::set<Item*>& getItems() const;
        static const std::set<Category*>& getList();
        const int getId() const;

        void addItem(Item* newItem);
        void removeItem(Item* oldItem);
};

std::ostream& operator<<(std::ostream& out, const Category& cat);

std::ostream& operator<<(std::ostream& out, const std::set<Category*> cat);

#endif //Category_H