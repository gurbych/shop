#include "Category.h"

std::set<Category*> Category::categoryList {};
int Category::lastCategoryId = 0;

Category::Category(const std::string& name) : name(name) {
    categoryList.insert(this);
    this->catId = lastCategoryId;
    lastCategoryId += 1;
}

Category::~Category() {
    std::set<Item*>::iterator it = items.begin();

    for ( ; it != items.end(); ++it ) {
        delete (*it);
    }
    items.clear();
}

const std::string& Category::getName() const {
    return name;
}

const std::set<Item*>& Category::getItems() const {
    return items;
}

const std::set<Category*>& Category::getList() {
    return categoryList;
}

const int Category::getId() const {
    return catId;
}

void Category::addItem(Item* newItem) {
    if ( newItem->getCat() != this ) {
        return;
    }
    items.insert(newItem);
}

void Category::removeItem(Item* oldItem) {
    items.erase(oldItem);
}

std::ostream& operator<<(std::ostream& out, const Category& cat) {
    out << cat.getName() << ":" << std::endl;

    std::set<Item*>::iterator it = cat.getItems().begin();
    for ( ; it != cat.getItems().end(); it++ ) {
        out << "--" << (*it)->getName() << std::endl;
    }
    return out;
}

std::ostream& operator<<(std::ostream& out, const std::set<Category*> cat) {
    std::set<Category*>::iterator it = cat.begin();

    out << "List of categories:" << '\n';
    for ( ; it != cat.end(); it++ ) {
        out << (*it)->getName() << " (ID: " << (*it)->getId() << ")" << std::endl;
    }
    return out;
}